opcion=int(input("escoja su opcion: \n1.palabra aleatoria \n2.ingresar palabra \n"))
if opcion == 1:
    palabra = 'hola'.split()
elif opcion == 2:
    palabra = input("ingrese su palabra :")
    muestra = ['_']*len(palabra)
print('A H O R C A D O')
print(muestra)
vidas = 7
while muestra != palabra:
    letras = input('ingrese su letra: ')
    letra = letras[0]
    if letra in palabra:
        contador = 0
        for letra_de_palabra in palabra:
            if  letra_de_palabra == letra:
                muestra[contador] = letra
            contador = contador + 1
        print(muestra)           
    else:
        vidas = vidas-1
        print(f'La letra no hace parte de la palabra.\nte quedan {vidas} vidas')
    if vidas == 0:
        print('perdiste')
        break
else:
    print('Ganaste')
