from dibujo import dibujo
def seleccionar_palabra():
    palabra = input("ingrese su palabra :")
    muestra = ['_']*len(palabra)
    return palabra,muestra

def pintar_muñeco(vidas,lista):
    if vidas == 7:
        print(lista[0])
    elif vidas == 6:
        print(lista[1])
    elif vidas == 5:
        print(lista[2])
    elif vidas == 4:
        print(lista[3])
    elif vidas == 3:
        print(lista[4])
    elif vidas == 2:
        print(lista[5])
    elif vidas == 1:
        print(lista[6])

def letras():
    letras = input('ingrese su letra: ')
    letra = letras[0]
    return letra

def verificar_letra(palabra,muestra,contador,letra):
    for letra_de_palabra in palabra:
        if  letra_de_palabra == letra:
            muestra[contador] = letra
        contador = contador + 1
    print(muestra)

def juego(lista):
    palabra,muestra = seleccionar_palabra()
    win = 0
    print('A H O R C A D O')
    print(muestra)
    vidas = 8
    while muestra != palabra:
        letra = letras()
        if letra in palabra:
            win = win +1
            contador = 0
            verificar_letra(palabra,muestra,contador,letra)
        else:
            vidas = vidas-1
            print(f'La letra no hace parte de la palabra.\nte quedan {vidas} vidas')
            pintar_muñeco(vidas,lista)
        if vidas == 0:
            print('Te quedaste sin vidas')
            break
        if win == len(palabra):
            print('Adivinaste la palabra')
            break

juego(dibujo)